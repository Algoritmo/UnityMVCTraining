﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beats {
    public class GamePlayController : MonoBehaviour {
        // singleton instance
        private static GamePlayController _instance;
        public static GamePlayController Instance {
            get {
                if (_instance == null)
                    _instance = (GamePlayController)GameObject.FindObjectOfType(typeof(GamePlayController));
                
                return _instance;
            }
        }

        
        [Header("Input")]
        [SerializeField] public KeyCode _left;
        [SerializeField] public KeyCode _right;
        [SerializeField] public KeyCode _up;
        [SerializeField] public KeyCode _down;

        [Header("Track")]
        [SerializeField] private Track _track;
        public Track track {
            get { return _track;  }
        }

        public float secondsPerBeat { get; private set; }
        public float beatsPerSecond { get; private set; }

        private bool _played;
        private bool _completed;

        private WaitForSeconds waitAndStop;

        TrackView _trackView;
        

        #region MonoBehavior Methods
        void Awake () {
            _instance = this;
            secondsPerBeat = track.bpm / 60;
            beatsPerSecond = 60f / track.bpm;
            
            waitAndStop = new WaitForSeconds(beatsPerSecond*2);

            _trackView = FindObjectOfType<TrackView>();

            // if _trackView is null
            if (!_trackView)
                Debug.LogWarning("No TrackView found in current scene.");
        }

        void Start() {
            InvokeRepeating("NextBeat", 0f, beatsPerSecond);
        }

        void Update() {
            if (_played || _completed)
                return;

            if ( Input.GetKeyDown(_left)) 
                PlayBeat(0);
            if (Input.GetKeyDown(_down))
                PlayBeat(1);
            if (Input.GetKeyDown(_up))
                PlayBeat(2);
            if (Input.GetKeyDown(_right))
                PlayBeat(3);
        }

        void OnDestroy () {
            _instance = null;
        }
        #endregion

        #region GamePlay
        private int _current;
        public int current {
            get { return _current; }
            set {
                if (value != _current)
                    _current = value;

                if ( _current == _track.beats.Count ) {
                    CancelInvoke("NextBeat");
                    _completed = true;

                    StartCoroutine(WaitAndStop());
                }
            }
        }

        void PlayBeat (int input) {
            //debug.log(input);
            _played = true;

            if ( _track.beats[current] == -1) {
                //Debug.Log(string.Format("{0} played untimely", input));

            } else if (_track.beats[current] == input) {
                //Debug.Log(string.Format("{0} played right", input));
                _trackView.TriggerBeatView(current, TrackView.Trigger.Right);

            } else {
                //Debug.Log(string.Format("{0} played, {1} expected", input, _track.beats[current]));
                _trackView.TriggerBeatView(current, TrackView.Trigger.Wrong);
            }

            
        }

        void NextBeat() {
            Debug.Log("Tick");

            if (!_played && _track.beats[current] != -1) {
                //Debug.Log(string.Format("{0} missed", _track.beats[current]));
                _trackView.TriggerBeatView(current, TrackView.Trigger.Missed);
            }

            _played = false;

            current++;
        }

        private IEnumerator WaitAndStop () {
            yield return waitAndStop;
            enabled = false;
        }

        #endregion
    }
}