﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Beats {
    [RequireComponent (typeof (VerticalLayoutGroup)) ]
    [RequireComponent (typeof (ContentSizeFitter))]
    [RequireComponent (typeof (RectTransform))]
    public class TrackView : MonoBehaviour {
        public enum Trigger { Missed, Right, Wrong };

        [SerializeField] RectTransform _left;
        [SerializeField] RectTransform _right;
        [SerializeField] RectTransform _up;
        [SerializeField] RectTransform _down;
        [SerializeField] RectTransform _empty;

        RectTransform _rectTransform;
        List<Image> _beatViews;

        Vector2 _position;
        float _beatViewSize;
        float _spacing;

        public float position {
            get { return _position.y; }
            set {
                if (value != _position.y) {
                    _position.y = value;
                    _rectTransform.anchoredPosition = _position;
                }
            }
        }

        public void Init(Track track) {
            _beatViews = new List<Image>();
            _rectTransform = (RectTransform) transform;
            _position = _rectTransform.anchoredPosition;

            _beatViewSize = _empty.rect.height;
            _spacing = GetComponent<VerticalLayoutGroup>().spacing;

            GameObject gameObject;

            foreach ( int beat in track.beats ) {
                switch ( beat ) {
                    case 0:
                        gameObject = _left.gameObject;
                        break;

                    case 1:
                        gameObject = _down.gameObject;
                        break;

                    case 2:
                        gameObject = _up.gameObject;
                        break;

                    case 3:
                        gameObject = _right.gameObject;
                        break;

                    default :
                        gameObject = _empty.gameObject;
                        break;
                }

                Image view = GameObject.Instantiate(gameObject, transform).transform.GetComponent<Image>();
                view.transform.SetAsFirstSibling();

                _beatViews.Add(view);
            }


        }

        void Start() {
            Init(GamePlayController.Instance.track);
        }

        void Update() {
            position -= (_beatViewSize + _spacing ) * Time.deltaTime * GamePlayController.Instance.secondsPerBeat;
        }

        public void TriggerBeatView ( int index, Trigger trigger ) {
            switch( trigger ) {
                case Trigger.Missed:
                    _beatViews[index].color = Color.gray;
                    //Debug.Break();
                    break;
                case Trigger.Right:
                    _beatViews[index].color = Color.yellow;
                    break;
                case Trigger.Wrong:
                    _beatViews[index].color = Color.cyan;
                    break;
            }
        }
    }
}